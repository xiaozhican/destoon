<?php defined('IN_DESTOON') or exit('Access Denied');?><div id="footerBox">
    <div class="footer">
            <!-- 此处换成广告位配置 --> 
            <div class="footerTop">
                <div class="gfNavItem gfLt">
                    
                        <img src="<?php echo DT_SKIN;?>img/wlogo.png" width="156">
                    <p class="tel"><b><?php echo $DT['telephone'];?></b></p>
                    <p class="date">周一至周五 9:00-18:00 </p>
                </div>
                <div class="gfNavItem">
                    <h4 class="gfNavTitle">新手指南</h4>
                    <ul>
                        <li><a href="#" target="_blank">免费注册</a></li><li><a href="#" target="_blank">采购商入门</a></li><li><a href="#" target="_blank">供应商入门</a></li><li><a href="#" target="_blank">找回密码</a></li><li><a href="#" target="_blank">帮助中心</a></li>
                    </ul>
                </div>
                <div class="gfNavItem">
                    <h4 class="gfNavTitle">商家合作</h4>
                    <ul>
                       <dd><a href="/member/login.php" target="_blank" rel="nofollow">商家入驻</a></dd>
                <dd><a  href="/ad" target="_blank" rel="nofollow">广告服务</a></dd>
                <dd><a  href="/jianzhan" target="_blank">企业建站</a></dd>
                    </ul>
                </div>
                <div class="gfNavItem">
                    <h4 class="gfNavTitle">关于我们</h4>
                    <ul>
                       <li><a href="/about/index.html" target="_blank" rel="nofollow">公司简介</a></li>
                <li><a href="/about/contact.html" target="_blank" rel="nofollow">联系我们</a></li>
                <li><a href="/about/zhaopin.html" target="_blank" rel="nofollow">人才招聘</a></li>
                    </ul>
                </div>
                <div class="fonterCode">
                    <h4 class="mobileTitle"></h4>
                    <p>手机88网：m.gx88.com</p>
                    <div class="codeBox">
                        <dl>
                            <dd>
                                <img src="<?php echo DT_SKIN;?>img/qrcode.png" width="68" height="68"></dd>
                            <dt>手机站</dt>
                        </dl>
                        <dl>
                            <dd>
                                <img src="<?php echo DT_SKIN;?>img/qrcode.png" width="68" height="68"></dd>
                            <dt>微信公众号</dt>
                        </dl>
                    </div>
                </div>
            </div>
        <ul class="fonterInfo">
            
                <li class="footerSortLine"></li>
                
            <li class="footCopyright"><div><?php echo $DT['copyright'];?> <?php if($DT['icpno']) { ?><a href="http://www.miibeian.gov.cn" target="_blank">通信管理局备案：<?php echo $DT['icpno'];?></a><?php } ?>
 </div></li>
          
        </ul>
    </div>
</div>
<div class="right-side-bar">
      <div class="r_top r_t">
          <ul>
              <li><a href="<?php echo $MODULE['2']['linkurl'];?>"><span><img src="<?php echo DT_SKIN;?>img/user.jpg" /></span><p>会员中心</p></a></li>
                <li><a href="/mall/cart.php"><span><img src="<?php echo DT_SKIN;?>img/cart.jpg" /></span><p>购物车</p></a></li>
                <li><a href="<?php echo $MODULE['2']['linkurl'];?>message.php"><span><img src="<?php echo DT_SKIN;?>img/mail.jpg" /></span><p>站内信</p></a></li>
                <li><a href="<?php echo $MODULE['2']['linkurl'];?>chat.php"><span><img src="<?php echo DT_SKIN;?>img/say.jpg" /></span><p>新对话</p></a></li>
        </ul>
    </div>
        <div class="r_top r_p">
          <ul>
              <li><a href="<?php echo $MODULE['2']['linkurl'];?>grade.php"><span><img src="<?php echo DT_SKIN;?>img/service.jpg" /></span><p>会员服务</p></a></li>
                <li><a href="<?php echo $MODULE['2']['linkurl'];?>my.php"><span><img src="<?php echo DT_SKIN;?>img/news.jpg" /></span><p>发布信息</p></a></li>
                <li id="goback1" onclick="window.scrollTo(0,0);"><a><span><img src="<?php echo DT_SKIN;?>img/top.jpg" /></span><p>回顶部</p></a></li>
          </ul>
        </div>
  </div>
<script type="text/javascript">
<?php if($destoon_task) { ?>
show_task('<?php echo $destoon_task;?>');
<?php } else { ?>
<?php include DT_ROOT.'/api/task.inc.php';?>
<?php } ?>
<?php if($lazy) { ?>$(function(){$("img").lazyload();});<?php } ?>
</script>
</body>
</html>